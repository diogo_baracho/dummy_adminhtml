<?php 
/**
 * Magento Dummys 
 * To Help You ;)
 *
 *  @author      Rafael Kisters ventura <kistters@gmail.com>
 */
class Dummy_Adminhtml_Model_Observer extends Varien_Event_Observer
{

    public function appendFieldsToCollection(Varien_Event_Observer $observer)
    {
        $name = $observer->getEvent()->getName();

        switch ($name) {
            case 'catalog_product_collection_load_before':
                Dummy_Adminhtml_Model_Grid_Product::appendFieldsToCollection($observer);
                break;
        }
    }
    /**
     *
     */
    public function appendColumnToGrid(Varien_Event_Observer $observer)
    {
        $block = $observer->getBlock();

        if (!isset($block)) {
            return $this;
        }

        switch ($block) {
            case $block instanceof Mage_Adminhtml_Block_Catalog_Product_Grid:
                Dummy_Adminhtml_Model_Grid_Product::appendColumnToGrid($block);
                break;

            case $block instanceof Mage_Adminhtml_Block_Promo_Quote_Grid:
                Dummy_Adminhtml_Model_Grid_Promotion::appendColumnToGrid($block);
                break;
        }
        
        return;
    }
}



 ?>