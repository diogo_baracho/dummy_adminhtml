<?php 
/**
 * Magento Dummys 
 * To Help You ;)
 *
 *  @author      Rafael Kisters ventura <kistters@gmail.com>
 */
class Dummy_Adminhtml_Model_Grid_Product
{

    protected static $_fields = array(
        'meta_keyword',
        'meta_title',
        'meta_description'
        );

    static public function appendFieldsToCollection(Varien_Event_Observer $observer)
    {
        if (!self::IsActive()) {
           return;
        }

        $collection = $observer->getCollection();
        $collection->addAttributeToSelect(self::$_fields);
    }

    static public function appendColumnToGrid(Mage_Adminhtml_Block_Catalog_Product_Grid $block)
    {
        if (!self::IsActive()) {
           return;
        }

        /* @var $block Mage_Adminhtml_Block_Catalog_Product_Grid */
        $block->addColumnAfter('Meta', array(
            'header'    => 'Meta Tag Empty',
            'width'     => '50',
            'sortable'  => false,
            'filter'    => false,
            'align'     => 'left',
            'index'     => 'icon',
            'frame_callback' => array(new self, 'MetaTagRenderer')
        ), 'is_active');

        return $block;
    }

    public function MetaTagRenderer($value, $row, $column, $isExport)
    {
        $keyword = (empty($row->getMetaKeyword()))? '<span class="grid-severity-major"><span>KeyWord</span></span>' : '';
        $title = (empty($row->getMetaTitle()))? '<span class="grid-severity-major"><span>Title</span></span>' : '';
        $description = (empty($row->getMetaDescription()))? '<span class="grid-severity-major"><span>Description</span></span>' : '';

        return  $keyword.$title.$description;
    }

    static public function IsActive(){
        return Mage::getStoreConfigFlag('dummyAdmin/grid/product');
    }
}